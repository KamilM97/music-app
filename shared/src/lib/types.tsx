export type FollowersType = {
  rank: number;
  artist: string;
  followers: number;
};

export type ListenersType = {
  rank: number;
  artist: string;
  monthlyListeners: number;
};

export type ChartDataType = {
  name: string;
  value: number;
};

export type ChartsListenersType = {
  artist: string;
  monthlyListeners: number;
};
