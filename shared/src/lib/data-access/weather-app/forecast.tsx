import { useQuery } from 'react-query';
import { CustomError } from '@my-workspace/shared';

export function useWeatherForecast(city: string, days: number) {
  return useQuery(
    ['weatherForecast', city, days],
    async () => {
      const headers = {
        'X-RapidAPI-Key': 'c526bc981fmshf3f6055ab108b0bp196421jsn8fc127a8c76a',
        'X-RapidAPI-Host': 'weatherapi-com.p.rapidapi.com',
      };
      const url = `https://weatherapi-com.p.rapidapi.com/forecast.json?q=${city}&days=${days.toString()}`;

      const response = await fetch(url.toString(), {
        headers,
      });

      if (!response.ok) {
        const error: CustomError = new Error('Network response was not ok');
        error.status = response.status;

        if (response.status === 400) {
          error.message = 'Błąd HTTP 400';
        } else if (response.status === 429) {
          error.message = 'Błąd HTTP 429';
        }

        throw error;
      }

      const data = await response.json();
      return data;
    },
    {
      onError: (error: Error) => {
        if (error.message.startsWith('Błąd HTTP 400')) {
          console.error('Obsługa błędu HTTP 400:', error.message);
        } else if (error.message.startsWith('Błąd HTTP 429')) {
          console.error('Obsługa błędu HTTP 429:', error.message);
        } else if (error.message === 'Network response was not ok') {
          console.error('Wystąpił inny błąd HTTP');
        }
      },
    }
  );
}
