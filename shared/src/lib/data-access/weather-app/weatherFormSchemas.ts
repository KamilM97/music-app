import { z } from 'zod';
import errors from '../../../../../apps/weather-app/translations/en/errors.json';

export const contactFormSchema = z.object({
  email: z.string().email(errors.invalidEmail),
  message: z.string().min(1, errors.messegeIsRequired),
});
