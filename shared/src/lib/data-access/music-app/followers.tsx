import { useQuery } from 'react-query';

export function useTopFollowers() {
  return useQuery('topFollowers', async () => {
    const response = await fetch(
      'https://spotify81.p.rapidapi.com/top_20_by_followers',
      {
        headers: {
          'X-RapidAPI-Key':
            'c526bc981fmshf3f6055ab108b0bp196421jsn8fc127a8c76a',
          'X-RapidAPI-Host': 'spotify81.p.rapidapi.com',
        },
      }
    );

    const data = await response.json();
    return data;
  });
}
