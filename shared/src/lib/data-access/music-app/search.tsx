import { useQuery } from 'react-query';

export function useSearch(
  inputSearch: string,
  category: string,
  limit: number
) {
  return useQuery('useSearch', async ({ pageParam = 1 }) => {
    const headers = {
      // 'X-RapidAPI-Key': 'b65a50d11dmshf00487acbdcaf91p1f6215jsn8a497fc6e311',
      'X-RapidAPI-Host': 'spotify23.p.rapidapi.com',
    };

    const url = new URL('https://spotify23.p.rapidapi.com/search/');
    url.searchParams.append('q', inputSearch);
    url.searchParams.append('type', category);
    url.searchParams.append('offset', `${(pageParam - 1) * limit}`);
    url.searchParams.append('limit', `${limit}`);
    url.searchParams.append('numberOfTopResults', '5');

    const response = await fetch(url.toString(), {
      headers,
    });

    if (!response.ok) {
      throw new Error('Network response was not ok');
    }

    const data = await response.json();
    return data;
  });
}
