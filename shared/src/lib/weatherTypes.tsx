import { Cell, Column } from 'react-table';

export type CoordinatesTypes = {
  lat: number;
  lon: number;
  onCoordinateChange: (newCoordinates: [number, number]) => void;
};

export type CustomError = Error & { status?: number };

export type ContactModalProps = {
  isOpen: boolean;
  onRequestClose: () => void;
};

export type FormErrors = {
  email?: string;
  message?: string;
};

export type TableColumn = Column & {
  Header: string;
  accessor: string;
  Cell?: Cell;
};

export type HourCondition = {
  icon: string;
};

export type HourData = {
  temp_c: number;
  wind_kph: number;
  chance_of_rain: number;
  humidity: number;
  condition: HourCondition;
};

interface TableData {
  forecast: {
    forecastday: {
      hour: {
        temp_c: number;
        wind_kph: number;
        chance_of_rain: number;
        humidity: number;
        condition: HourCondition;
      }[];
    }[];
  };
}

export type TableComponentProps = {
  columns: TableColumn[];
  data: TableData;
  daysDifference: number | null;
};
