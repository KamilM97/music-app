import 'tailwindcss/tailwind.css';

import React from 'react';
import ReactDOM from 'react-dom/client';
import { QueryClient, QueryClientProvider } from 'react-query';

import './index.css';

import Navbar from './app/Navbar';
import Library from './app/Library';
import { BrowserRouter as Router } from 'react-router-dom';

import { I18nextProvider } from 'react-i18next';
import i18n from '../i18n';

const queryClient = new QueryClient();

const storedLanguage = localStorage.getItem('language');
i18n.changeLanguage(storedLanguage || 'en');

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

root.render(
  <React.StrictMode>
    <I18nextProvider i18n={i18n}>
      <QueryClientProvider client={queryClient}>
        <Router>
          <div className="flex flex-col w-full items-center">
            <div className="flex justify-center">
              <h1 className="lg:text-5xl text-3xl my-10 font-bold bg-third-purple text-white py-6 px-10 rounded-full max-w-max">
                Music App Project
              </h1>
            </div>
            <div className="flex lg:flex-row flex-col justify-center lg:w-4/6 w-full">
              <Navbar />
              <Library />
            </div>
          </div>
        </Router>
      </QueryClientProvider>
    </I18nextProvider>
  </React.StrictMode>
);
