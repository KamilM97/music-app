import { ListenersType } from '@my-workspace/shared';

import { useTranslation } from 'react-i18next';
import '../../../i18n';

function FollowersContainer({ rank, artist, monthlyListeners }: ListenersType) {
  const { t } = useTranslation('common');

  return (
    <div
      data-testid={`listeners-container-${rank}`}
      className="p-2 lg:pl-3 pl-5"
    >
      <p className="pt-1 font-bold truncate">
        {rank}.{' '}
        <a href={`https://open.spotify.com/search/${artist}`}>{artist}</a>
      </p>
      <p className="truncate text-gray-400">
        {t('followers')}: {monthlyListeners}
      </p>
    </div>
  );
}

export default FollowersContainer;
