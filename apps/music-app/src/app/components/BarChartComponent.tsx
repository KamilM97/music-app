import {
  Tooltip,
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  ResponsiveContainer,
} from 'recharts';

import { ChartDataType } from '@my-workspace/shared';

function BarChartComponent({ chartData }: { chartData: ChartDataType[] }) {
  return (
    <ResponsiveContainer width="100%" height={400}>
      <BarChart
        data={chartData}
        margin={{
          top: 5,
          right: 30,
          left: 20,
          bottom: 100,
        }}
        barSize={20}
      >
        <XAxis
          dataKey="name"
          scale="point"
          padding={{ left: 10, right: 10 }}
          angle={-90}
          textAnchor="end"
        />
        <YAxis />
        <Tooltip
          cursor={{ fill: 'transparent' }}
          content={({ payload }) => {
            if (payload && payload.length > 0) {
              return (
                <div className="custom-tooltip">
                  <p>{payload[0].payload.name}</p>
                  <p>{payload[0].payload.value}</p>
                </div>
              );
            }
            return null;
          }}
        />
        <CartesianGrid strokeDasharray="3 3" />
        <Bar dataKey="value" fill="#8884d8" background={{ fill: '#0a071e' }} />
      </BarChart>
    </ResponsiveContainer>
  );
}

export default BarChartComponent;
