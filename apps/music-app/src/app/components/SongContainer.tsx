import { useTranslation } from 'react-i18next';
import '../../../i18n';

function SongContainer() {
  const { t } = useTranslation('common');

  return (
    <div className="flex flex-col lg:px-1 lg:pt-4 lg:w-3/12 p-4">
      <img
        className="rounded-xl"
        src="src/assets/images/Album.jpg"
        alt="Thumbnail"
      />
      <p className="pt-1 font-bold truncate">{t('songName')}</p>
      <p className="truncate text-gray-400">{t('authorName')}</p>
    </div>
  );
}

export default SongContainer;
