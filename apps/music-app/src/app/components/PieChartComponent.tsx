import { PieChart, Pie, Tooltip } from 'recharts';

import { ChartDataType } from '@my-workspace/shared';

function PieChartComponent({ chartData }: { chartData: ChartDataType[] }) {
  return (
    <PieChart width={400} height={400}>
      <Pie
        dataKey="value"
        isAnimationActive={false}
        data={chartData}
        cx="50%"
        cy="50%"
        outerRadius={80}
        fill="#8884d8"
        label={{
          fill: 'gray',
        }}
      />
      <Tooltip
        contentStyle={{
          backgroundColor: '#8884d8',
        }}
      />
    </PieChart>
  );
}

export default PieChartComponent;
