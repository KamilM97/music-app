import { useState, useEffect } from 'react';

import { useTranslation } from 'react-i18next';
import '../../i18n';

import { useSearch } from '@my-workspace/shared';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons';
import PlaceholderImage from '../assets/images/PlaceholderImage.jpg';

function AppListeners() {
  const [searchValue, setSearchValue] = useState<string>('Eminem');
  const [selectedCategory, setSelectedCategory] = useState<string>('artists');
  const [searchToSubmit, setSearchToSubmit] = useState('Eminem');
  const [loadedResults, setLoadedResults] = useState(20);

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchValue(event.target.value);
  };

  const handleSelectChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    setSelectedCategory(event.target.value);
    setLoadedResults(20);
  };

  const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      setSearchToSubmit(searchValue);
    }
  };

  const handleIconClick = () => {
    setSearchToSubmit(searchValue);
  };

  const loadMoreResults = () => {
    setLoadedResults((prevLoadedResults) => prevLoadedResults + 20);
  };

  const { data, error, isLoading } = useSearch(
    searchToSubmit,
    selectedCategory,
    loadedResults
  );

  const [totalCount, setTotalCount] = useState(data?.artists?.totalCount);

  const { t } = useTranslation('common');

  useEffect(() => {
    if (selectedCategory === 'genres') {
      setTotalCount(data?.genres?.totalCount);
    } else if (selectedCategory === 'artists') {
      setTotalCount(data?.artists?.totalCount);
    } else if (selectedCategory === 'albums') {
      setTotalCount(data?.albums?.totalCount);
    } else if (selectedCategory === 'podcasts') {
      setTotalCount(data?.podcasts?.totalCount);
    } else if (selectedCategory === 'tracks') {
      setTotalCount(data?.tracks?.totalCount);
    }
  }, [data, selectedCategory]);

  //Data
  if (isLoading) {
    return <div className="text-white text-lg">{t('loading')}...</div>;
  }

  if (error) {
    return (
      <div className="text-white text-lg">
        {t('error')}: {'Error'}
      </div>
    );
  }

  const albums = data?.albums?.items || [];
  const artists = data?.artists?.items || [];
  const genres = data?.genres?.items || [];
  const podcasts = data?.podcasts?.items || [];
  const tracks = data?.tracks?.items || [];

  return (
    <>
      <div className="border-solid border-gray-400 border-b-2 pb-4">
        <h2 className="lg:pl-3 pl-5 text-2xl font-semibold pb-1">
          {t('library')}
        </h2>
        <div className="lg:pl-3 flex flex-row pl-5 text-gray-400 font-semibold text-lg">
          <p className="">{t('searchBy')}</p>
          <select
            className="ml-4 text-white bg-indigo-950"
            id="categories"
            name="categories"
            value={selectedCategory}
            onChange={handleSelectChange}
          >
            <option value="albums">{t('albums')}</option>
            <option value="artists">{t('artists')}</option>
            <option value="genres">{t('genres')}</option>
            <option value="podcasts">{t('podcasts')}</option>
            <option value="tracks">{t('tracks')}</option>
          </select>
        </div>
        <div className="lg:mx-0 mx-5 relative mt-4">
          <input
            className="bg-dark-gray text-gray-500 placeholder-gray-500 w-full rounded-lg p-1 text-indent"
            type="text"
            placeholder="Search"
            value={searchValue}
            onChange={handleInputChange}
            onKeyDown={handleKeyPress}
          ></input>
          <FontAwesomeIcon
            className="absolute top-1/4 left-2 text-gray-500 cursor-pointer"
            icon={faMagnifyingGlass}
            onClick={handleIconClick}
          />
        </div>

        <p className="lg:pl-3 pl-5 pt-3 text-gray-400">
          {totalCount !== undefined ? `Results: ${totalCount}` : null}
        </p>
      </div>

      <div>
        {(() => {
          switch (selectedCategory) {
            case 'albums':
              return albums?.length > 0 ? (
                <>
                  <ul className="flex flex-row flex-wrap">
                    {albums?.map((album: any, index: number) => (
                      <li
                        className="flex flex-col lg:px-1 lg:pt-4 lg:w-3/12 p-4"
                        key={index}
                      >
                        <a
                          href={`https://open.spotify.com/album/${
                            album?.data?.uri.split(':')[2]
                          }`}
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          {album?.data?.coverArt?.sources ? (
                            <img
                              className="lg:max-h-32 rounded-xl"
                              src={album?.data.coverArt.sources[0].url}
                              alt={album?.data.name}
                            />
                          ) : (
                            <img
                              className="lg:max-h-32 rounded-xl"
                              src={PlaceholderImage}
                              alt="placeholder image"
                            />
                          )}

                          <p className="text-center pt-1 font-bold truncate">
                            {album?.data?.name || 'Unknown Album'}
                          </p>
                          <p className="text-center truncate text-gray-400">
                            {album?.data?.artists?.items[0]?.profile?.name ||
                              'Unknown Artist'}
                          </p>
                        </a>
                      </li>
                    ))}
                  </ul>
                  {data?.albums?.totalCount > loadedResults && (
                    <div className="flex w-full justify-center py-6">
                      <button
                        className="bg-indigo-950 rounded-xl px-6 py-4 cursor-pointer"
                        onClick={loadMoreResults}
                      >
                        {t('showMore')}
                      </button>
                    </div>
                  )}
                </>
              ) : (
                <p className="text-white text-lg p-4">{t('noAlbumsFound')}.</p>
              );

            case 'artists':
              return artists.length > 0 ? (
                <>
                  <ul className="flex flex-row flex-wrap">
                    {artists?.map((artist: any, index: number) => (
                      <li
                        className="flex flex-col  lg:px-1 lg:pt-4 lg:w-3/12 p-4"
                        key={index}
                      >
                        <a
                          href={`https://open.spotify.com/artist/${
                            artist?.data?.uri.split(':')[2]
                          }`}
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          {artist?.data?.visuals?.avatarImage?.sources ? (
                            <img
                              className="lg:max-h-32 rounded-xl"
                              src={
                                artist.data.visuals.avatarImage.sources[0].url
                              }
                              alt={artist.data.profile.name}
                            />
                          ) : (
                            <img
                              className="lg:max-h-32 rounded-xl"
                              src={PlaceholderImage}
                              alt="placeholder image"
                            />
                          )}
                          <p className="text-center pt-1 font-bold truncate">
                            {artist?.data?.profile?.name || 'Unknown'}
                          </p>
                        </a>
                      </li>
                    ))}
                  </ul>
                  {data?.artists?.totalCount > loadedResults && (
                    <div className="flex w-full justify-center py-6">
                      <button
                        className="bg-indigo-950 rounded-xl px-6 py-4 cursor-pointer"
                        onClick={loadMoreResults}
                      >
                        {t('showMore')}
                      </button>
                    </div>
                  )}
                </>
              ) : (
                <p className="text-white text-lg p-4">{t('noArtistsFound')}.</p>
              );

            case 'genres':
              return genres?.length > 0 ? (
                <>
                  <ul className="flex flex-row flex-wrap">
                    {genres?.map((genre: any, index: number) => (
                      <li
                        className="flex flex-col lg:px-1 lg:pt-4 lg:w-3/12 p-4"
                        key={index}
                      >
                        <a
                          href={`https://open.spotify.com/genre/${
                            genre?.data?.uri.split(':')[2]
                          }`}
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          {genre?.data?.image?.sources ? (
                            <img
                              className="lg:max-h-32 rounded-xl"
                              src={genre?.data.image.sources[0].url}
                              alt={genre?.data.name}
                            />
                          ) : (
                            <img
                              className="lg:max-h-32 rounded-xl"
                              src={PlaceholderImage}
                              alt="placeholder image"
                            />
                          )}

                          <p className="text-center pt-1 font-bold truncate">
                            {genre?.data?.name || 'Unknown Genre'}
                          </p>
                        </a>
                      </li>
                    ))}
                  </ul>
                  {data?.genres?.totalCount > loadedResults && (
                    <div className="flex w-full justify-center py-6">
                      <button
                        className="bg-indigo-950 rounded-xl px-6 py-4 cursor-pointer"
                        onClick={loadMoreResults}
                      >
                        {t('showMore')}
                      </button>
                    </div>
                  )}
                </>
              ) : (
                <p className="text-white text-lg p-4">{t('noGenresFound')}.</p>
              );

            case 'podcasts':
              return podcasts?.length > 0 ? (
                <>
                  <ul className="flex flex-row flex-wrap">
                    {podcasts?.map((podcast: any, index: number) => (
                      <li
                        className="flex flex-col lg:px-1 lg:pt-4 lg:w-3/12 p-4"
                        key={index}
                      >
                        <a
                          href={`https://open.spotify.com/show/${
                            podcast?.data?.uri.split(':')[2]
                          }`}
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          {podcast?.data?.coverArt?.sources ? (
                            <img
                              className="lg:h-32 l rounded-xl"
                              src={podcast?.data.coverArt.sources[0].url}
                              alt={podcast?.data.name}
                            />
                          ) : (
                            <img
                              className="lg:max-h-32 rounded-xl"
                              src={PlaceholderImage}
                              alt="placeholder image"
                            />
                          )}

                          <p className="text-center pt-1 font-bold truncate">
                            {podcast?.data?.name || 'Unknown Podcast'}
                          </p>
                          <p className="text-center truncate text-gray-400">
                            {podcast?.data?.mediaType || 'Unknown Type'}
                          </p>
                        </a>
                      </li>
                    ))}
                  </ul>
                  {data?.podcasts?.totalCount > loadedResults && (
                    <div className="flex w-full justify-center py-6">
                      <button
                        className="bg-indigo-950 rounded-xl px-6 py-4 cursor-pointer"
                        onClick={loadMoreResults}
                      >
                        {t('showMore')}
                      </button>
                    </div>
                  )}
                </>
              ) : (
                <p className="text-white text-lg p-4">
                  {t('noPodcastsFound')}.
                </p>
              );

            case 'tracks':
              return tracks?.length > 0 ? (
                <>
                  <ul className="flex flex-row flex-wrap">
                    {tracks?.map((track: any, index: number) => (
                      <li
                        className="flex flex-col  lg:px-1 lg:pt-4 lg:w-3/12 p-4"
                        key={index}
                      >
                        <a
                          href={`https://open.spotify.com/track/${
                            track?.data?.uri.split(':')[2]
                          }`}
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          {track?.data?.albumOfTrack?.coverArt?.sources ? (
                            <img
                              className="lg:max-h-32 rounded-xl "
                              src={
                                track.data.albumOfTrack.coverArt.sources[0].url
                              }
                              alt={track.data.name}
                            />
                          ) : (
                            <img
                              className="lg:max-h-32 rounded-xl"
                              src={PlaceholderImage}
                              alt="placeholder image"
                            />
                          )}

                          <p className="text-center pt-1 font-bold truncate">
                            {track?.data?.name || 'Unknown Track'}
                          </p>
                          <p className="text-center truncate text-gray-400">
                            {track?.data?.artists?.items[0]?.profile?.name ||
                              'Unknown Artist'}
                            {console.log(data.tracks.items)}
                          </p>
                        </a>
                      </li>
                    ))}
                  </ul>
                  <div className="flex w-full justify-center py-6">
                    <button
                      className="bg-indigo-950 rounded-xl px-6 py-4 cursor-pointer"
                      onClick={loadMoreResults}
                    >
                      {t('showMore')}
                    </button>
                  </div>
                </>
              ) : (
                <p className="text-white text-lg p-4">{t('noTracksFound')}.</p>
              );

            default:
              return null;
          }
        })()}
      </div>
    </>
  );
}

export default AppListeners;
