import { useEffect } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faMusic,
  faTrophy,
  faChartLine,
} from '@fortawesome/free-solid-svg-icons';
import Flag from 'react-country-flag';

import { NavLink, useNavigate, useLocation } from 'react-router-dom';

import { useTranslation } from 'react-i18next';
import i18n from '../../i18n';

function Navbar() {
  const { t } = useTranslation('common');
  const navigate = useNavigate();
  const location = useLocation();

  const changeLanguage = (language: string) => {
    i18n.changeLanguage(language);

    const currentPath = window.location.pathname;
    const newPath = currentPath.replace(/\/(en|pl)\//, `/${language}/`);
    navigate(newPath);
  };

  useEffect(() => {
    const pathParts = location.pathname.split('/');
    const langFromUrl = pathParts[1];
    const supportedLanguages = ['en', 'pl'];

    if (supportedLanguages.includes(langFromUrl)) {
      i18n.changeLanguage(langFromUrl);
    }
  }, [location.pathname]);

  return (
    <div className="lg:w-3/12 lg:p-3 lg:pb-6 lg:pt-8 lg:pl-10 lg:rounded-l-3xl lg:rounded-r-none bg-dark-purple text-white px-10 py-6 rounded-t-3xl">
      <div>
        <h2 className="text-2xl font-semibold pb-1">{t('categories')}</h2>
      </div>
      <div>
        <ul>
          <li className="mt-4">
            <NavLink to={`/${i18n.language}/search`} end>
              <FontAwesomeIcon className="lg:mr-4 mr-2" icon={faMusic} />
              {t('search')}
            </NavLink>
          </li>
          <li className="mt-4">
            <NavLink to={`/${i18n.language}/dashboard`} end>
              <FontAwesomeIcon className="lg:mr-4 mr-2" icon={faChartLine} />
              {t('dashboard')}
            </NavLink>
          </li>
          <li className="mt-4">
            <NavLink to={`/${i18n.language}/followers`} end>
              <FontAwesomeIcon className="lg:mr-4 mr-2" icon={faTrophy} />
              {t('topByFollowers')}
            </NavLink>
          </li>
          <li className="mt-4">
            <NavLink to={`/${i18n.language}/listeners`} end>
              <FontAwesomeIcon className="lg:mr-4 mr-2" icon={faTrophy} />
              {t('topByMonthlyListeners')}
            </NavLink>
          </li>
        </ul>
        <div className="mt-8 flex">
          <button onClick={() => changeLanguage('en')}>
            <Flag countryCode="US" svg />
          </button>
          <p className="px-1">/</p>
          <button onClick={() => changeLanguage('pl')}>
            <Flag countryCode="PL" svg />
          </button>
        </div>
      </div>
    </div>
  );
}

export default Navbar;
