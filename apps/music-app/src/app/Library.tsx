import { Route, Routes } from 'react-router-dom';
import AppFollowers from './AppFollowers';
import AppListeners from './AppListeners';
import AppSearch from './AppSearch';
import AppDashboard from './AppDashboard';

function Library() {
  return (
    <div className="lg:w-9/12 lg:p-4 lg:pt-8 lg:pr-5 lg:rounded-r-3xl lg:rounded-l-none w-full bg-secondary-purple text-white p-4 rounded-b-3xl min-h-screen">
      <Routes>
        <Route path="/:lang/search" element={<AppSearch />} />
        <Route path="/:lang/dashboard" element={<AppDashboard />} />
        <Route path="/:lang/followers" element={<AppFollowers />} />
        <Route path="/:lang/listeners" element={<AppListeners />} />
      </Routes>
    </div>
  );
}

export default Library;
