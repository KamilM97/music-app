import { useState } from 'react';

import { useTranslation } from 'react-i18next';
import '../../i18n';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faRightLong, faLeftLong } from '@fortawesome/free-solid-svg-icons';

import { useTopFollowers, FollowersType } from '@my-workspace/shared';
import FollowersContainer from './components/FollowersContainer';

function AppFollowers() {
  const { data, error, isLoading } = useTopFollowers();

  const { t } = useTranslation('common');

  let resultsCount = data?.length;

  //Pagination
  const [currentPage, setCurrentPage] = useState(1);
  const pageSize = 20;
  const startIndex = (currentPage - 1) * pageSize;
  const endIndex = startIndex + pageSize;
  let totalPages = Math.ceil(resultsCount / pageSize);
  totalPages--;

  //filter img in artists list
  const filteredData = data?.filter((item: FollowersType | undefined) => {
    if (typeof item === 'undefined') return false;
    const artist = item.artist.toLowerCase();
    if (artist.includes('<img')) {
      resultsCount--;
      return false;
    }
    return true;
  });
  const displayedData = filteredData?.slice(startIndex, endIndex);

  const handlePrevPage = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  };

  const handleNextPage = () => {
    if (endIndex < (resultsCount || 0)) {
      setCurrentPage(currentPage + 1);
    }
  };

  const handlePageInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newPage = parseInt(e.target.value, 10);
    if (!isNaN(newPage) && newPage >= 1 && newPage <= totalPages) {
      setCurrentPage(newPage);
    }
  };

  //Data
  if (isLoading) {
    return <div className="text-white text-lg">{t('loading')}...</div>;
  }

  if (error) {
    return (
      <div className="text-white text-lg">
        {t('error')}: {'Error'}
      </div>
    );
  }

  return (
    <>
      <div className="border-solid border-gray-400 border-b-2 pb-4">
        <h2 className="lg:pl-3 pl-5 text-2xl font-semibold pb-1">
          {t('library')}
        </h2>
        <p className="lg:pl-3 pl-5 text-gray-400 font-semibold text-lg">
          {t('topByFollowers')}
        </p>
        <p className="lg:pl-3 pl-5 pt-3 text-gray-400">
          {t('artists')}: {resultsCount}
        </p>
      </div>

      <ul>
        {displayedData
          ?.filter((item: FollowersType | undefined) => {
            if (typeof item === 'undefined') return false;
            const artist = item.artist.toLowerCase();
            return !artist.includes('<img');
          })
          .map((track: FollowersType) => (
            <li key={track.rank}>
              <FollowersContainer
                rank={track.rank}
                artist={track.artist}
                followers={track.followers}
              />
            </li>
          ))}
      </ul>

      <div className="lg:p-4 pt-6 px-5 flex justify-between w-full text-gray-400">
        <button onClick={handlePrevPage} disabled={currentPage === 1}>
          <FontAwesomeIcon className="mr-2 text-white" icon={faLeftLong} />
          {t('previous')}
        </button>

        <div className="text-white">
          <input
            type="number"
            value={currentPage}
            onChange={handlePageInputChange}
            min="1"
            max={totalPages}
            className="w-8 bg-indigo-950 text-center"
          />
          /{totalPages}
        </div>

        <button
          onClick={handleNextPage}
          disabled={endIndex >= (resultsCount || 0)}
        >
          {t('next')}
          <FontAwesomeIcon className="ml-2 text-white" icon={faRightLong} />
        </button>
      </div>
    </>
  );
}

export default AppFollowers;
