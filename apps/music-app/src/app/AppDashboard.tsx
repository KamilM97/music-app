import { useMemo } from 'react';

import { useTopListeners } from '@my-workspace/shared';

import { useTranslation } from 'react-i18next';
import PieChartComponent from './components/PieChartComponent';
import BarChartComponent from './components/BarChartComponent';

import { ChartsListenersType } from '@my-workspace/shared';

function AppDashboard() {
  const { data, error, isLoading } = useTopListeners();

  const { t } = useTranslation('common');

  const top10Artists = data?.slice(0, 10) || [];

  const chartData = useMemo(() => {
    return top10Artists.map((artist: ChartsListenersType) => ({
      name: artist.artist,
      value: artist.monthlyListeners,
    }));
  }, [top10Artists]);

  //Data
  if (isLoading) {
    return <div className="text-white text-lg">{t('loading')}...</div>;
  }

  if (error) {
    return (
      <div className="text-white text-lg">
        {t('error')}: {'Error'}
      </div>
    );
  }

  return (
    <div>
      <div className="flex flex-col items-center">
        <h3 className="text-2xl text-gray-400">{t('listenersPieChart')}</h3>
        <PieChartComponent chartData={chartData} />
      </div>

      <div className="flex flex-col items-center mb-10">
        <h3 className="text-2xl my-20 text-gray-400">
          {t('listenersBarChart')}
        </h3>
        <BarChartComponent chartData={chartData} />
      </div>
    </div>
  );
}

export default AppDashboard;
