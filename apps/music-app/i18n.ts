import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import commonEN from './translations/en/common.json';
import commonPL from './translations/pl/common.json';

const resources = {
  en: {
    common: { ...commonEN },
  },
  pl: {
    common: { ...commonPL },
  },
};

i18n.use(initReactI18next).init({
  resources,
  lng: 'en',
  fallbackLng: 'en',
  defaultNS: 'common',
  ns: ['common'],
  interpolation: {
    escapeValue: false,
  },
});

export default i18n;
