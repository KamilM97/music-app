import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import commonEN from './translations/en/common.json';
import errorsEN from './translations/en/errors.json';

const resources = {
  en: {
    common: { ...commonEN },
    errors: { ...errorsEN },
  },
};

i18n.use(initReactI18next).init({
  resources,
  lng: 'en',
  fallbackLng: 'en',
  defaultNS: 'common',
  ns: ['common', 'errors'],
  interpolation: {
    escapeValue: false,
  },
});

export default i18n;
