import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faWind,
  faStopwatch,
  faSun,
  faDroplet,
  faEye,
  faCalendar,
  faLocationDot,
} from '@fortawesome/free-solid-svg-icons';

import { useTranslation } from 'react-i18next';
import '../../i18n';

interface WeatherConditionProps {
  icon: any;
  temp: string;
  text: string;
  name: string;
  time: string;
  wind: string;
  pressure: string;
  humidity: string;
  vission: string;
  sunrise: string;
  sunset: string;
  firstDayFirstDate: string;
  firstDaySecondDate: string;
  secondDayFirstDate: string;
  secondDaySecondDate: string;
  firstDayFirstTemp: string;
  firstDaySecondTemp: string;
  secondDayFirstTemp: string;
  secondDaySecondTemp: string;
}

function WeatherConditionComponent({
  icon,
  temp,
  text,
  name,
  time,
  wind,
  pressure,
  humidity,
  vission,
  sunrise,
  sunset,
  firstDayFirstDate,
  firstDaySecondDate,
  secondDayFirstDate,
  secondDaySecondDate,
  firstDayFirstTemp,
  firstDaySecondTemp,
  secondDayFirstTemp,
  secondDaySecondTemp,
}: WeatherConditionProps) {
  const { t } = useTranslation('common');

  return (
    <>
      <div className="flex w-full">
        <div className="flex justify-center items-center flex-col second-bg w-3/12 rounded-lg mr-3">
          <div className="flex justify-center items-center flex-col h-8/12 w-2/3 border-solid border-b border-white">
            <img className="mt-3 w-1/2" src={icon} alt="Weather Icon" />
            <p className="w-1/4 text-3xl">{temp}°C</p>
            <p className="my-2">{text}</p>
          </div>
          <div className="flex flex-col w-2/3 h-4/12 mt-2">
            <div className="flex">
              <FontAwesomeIcon className="mr-3" icon={faLocationDot} />
              {name}
            </div>
            <div className="mt-2">
              <FontAwesomeIcon className="mr-3" icon={faCalendar} />
              {time}
            </div>
          </div>
        </div>
        <div className="flex justify-center items-center main-bg w-6/12">
          <div className="flex justify-center items-center flex-col w-6/12 mr-3">
            <div className="flex items-center second-bg w-full rounded-lg mb-3 py-5">
              <div className="flex justify-center w-4/12">
                <FontAwesomeIcon className="text-2xl" icon={faWind} />
              </div>
              <div className="flex flex-col w-8/12 pl-4">
                <p className="text-gray-400">{t('windSpeed')}</p>
                <p className="text-lg">
                  {wind} {t('km/h')}
                </p>
              </div>
            </div>
            <div className="flex items-center second-bg w-full rounded-lg mb-3 py-5">
              <div className="flex justify-center w-4/12">
                <FontAwesomeIcon className="text-2xl" icon={faStopwatch} />
              </div>
              <div className="flex flex-col w-8/12 pl-4">
                <p className="text-gray-400">{t('pressure')}</p>
                <p className="text-lg">
                  {pressure} {t('hPa')}
                </p>
              </div>
            </div>
            <div className="flex items-center second-bg w-full rounded-lg py-5">
              <div className="flex justify-center w-4/12">
                <FontAwesomeIcon className="text-2xl" icon={faSun} />
              </div>
              <div className="flex flex-col w-8/12 pl-4">
                <p className="text-gray-400">{t('sunrise')}</p>
                <p className="text-lg">{sunrise}</p>
              </div>
            </div>
          </div>
          <div className="flex justify-center items-center flex-col w-6/12">
            <div className="flex items-center second-bg w-full rounded-lg mb-3 py-5">
              <div className="flex justify-center w-4/12">
                <FontAwesomeIcon className="text-2xl" icon={faDroplet} />
              </div>
              <div className="flex flex-col w-8/12 pl-4">
                <p className="text-gray-400">{t('humidity')}</p>
                <p className="text-lg">{humidity} %</p>
              </div>
            </div>
            <div className="flex items-center second-bg w-full rounded-lg mb-3 py-5">
              <div className="flex justify-center w-4/12">
                <FontAwesomeIcon className="text-2xl" icon={faEye} />
              </div>
              <div className="flex flex-col w-8/12 pl-4">
                <p className="text-gray-400">{t('visibility')}</p>
                <p className="text-lg">
                  {vission} {t('km')}
                </p>
              </div>
            </div>
            <div className="flex items-center second-bg w-full rounded-lg py-5">
              <div className="flex justify-center w-4/12">
                <FontAwesomeIcon className="text-2xl" icon={faSun} />
              </div>
              <div className="flex flex-col w-8/12 pl-4">
                <p className="text-gray-400">{t('sunset')}</p>

                <p className="text-lg">{sunset}</p>
              </div>
            </div>
          </div>
        </div>
        <div className="flex justify-center items-center flex-col second-bg w-3/12 rounded-lg ml-3">
          <div className="flex justify-around items-center w-full h-1/4">
            <div>
              {firstDayFirstDate}
              <br />
              6:00
            </div>
            <p className="text-xl">{firstDayFirstTemp}°C</p>
          </div>
          <div className="flex justify-around items-center w-full h-1/4">
            <div>
              {firstDaySecondDate} <br />
              18:00
            </div>
            <p className="text-xl">{firstDaySecondTemp}°C</p>
          </div>
          <div className="flex justify-around items-center w-full h-1/4">
            <div>
              {secondDayFirstDate}
              <br /> 6:00
            </div>
            <p className="text-xl">{secondDayFirstTemp}°C</p>
          </div>
          <div className="flex justify-around items-center w-full h-1/4">
            <div>
              {secondDaySecondDate}
              <br /> 18:00
            </div>
            <p className="text-xl">{secondDaySecondTemp}°C</p>
          </div>
        </div>
      </div>
    </>
  );
}

export default WeatherConditionComponent;
