import LeafletMap from './LeafletMap';

import { CoordinatesTypes } from '@my-workspace/shared';

import { useTranslation } from 'react-i18next';
import '../../i18n';

const MapComponent: React.FC<CoordinatesTypes> = ({
  lat,
  lon,
  onCoordinateChange,
}) => {
  const { t } = useTranslation('common');

  return (
    <div className="flex flex-col main-color rounded-lg main-bg w-11/12 py-6 px-10 mb-16">
      <h2 className="text-2xl mb-10">{t('weatherByCoordinates')}</h2>
      <LeafletMap lat={lat} lon={lon} onCoordinateChange={onCoordinateChange} />
    </div>
  );
};

export default MapComponent;
