import { useTable } from 'react-table';

import { HourData, TableComponentProps } from '@my-workspace/shared';

const TableComponent: React.FC<TableComponentProps> = ({
  columns,
  data,
  daysDifference,
}) => {
  const forecastIndex =
    daysDifference !== null && daysDifference !== undefined
      ? daysDifference
      : 0;

  const tableData =
    (data &&
      data.forecast.forecastday[forecastIndex]?.hour.map(
        (hour: HourData, index: number) => ({
          hour: index,
          temp_c: `${hour.temp_c} °C`,
          wind_kph: `${hour.wind_kph} km/h`,
          chance_of_rain: `${hour.chance_of_rain} %`,
          humidity: `${hour.humidity} %`,
          icon: <img className="mx-auto" src={hour.condition.icon} alt="" />,
        })
      )) ||
    [];

  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
    useTable({
      columns,
      data: tableData,
    });

  return (
    <>
      <table
        {...getTableProps()}
        className="w-full border-2 border-white border-solid"
      >
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th
                  {...column.getHeaderProps()}
                  className="bg-blue-900 py-4 border-4 border-white border-solid"
                >
                  {column.render('Header')}
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {rows.map((row, rowIndex) => {
            prepareRow(row);
            return (
              <tr
                {...row.getRowProps()}
                className={`hover:bg-gray-400 hover:text-black ${
                  rowIndex % 2 === 0 ? 'bg-gray-800' : ''
                }`}
              >
                {row.cells.map((cell) => {
                  return (
                    <td
                      {...cell.getCellProps()}
                      className="text-center table-border"
                    >
                      {cell.render('Cell')}
                    </td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
};

export default TableComponent;
