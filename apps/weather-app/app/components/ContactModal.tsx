import React, { useState } from 'react';
import Modal from 'react-modal';
import { ZodError } from 'zod';

import {
  ContactModalProps,
  contactFormSchema,
  FormErrors,
} from '@my-workspace/shared';

import { useTranslation } from 'react-i18next';
import '../../i18n';

const ContactModal: React.FC<ContactModalProps> = ({
  isOpen,
  onRequestClose,
}) => {
  const [email, setEmail] = useState<string>('');
  const [message, setMessage] = useState<string>('');
  const [characterLimitError, setCharacterLimitError] = useState<string>('');
  const [formErrors, setFormErrors] = useState<FormErrors | null>(null);

  const MAX_CHARACTER_LIMIT = 12;

  const { t } = useTranslation(['common', 'errors']);

  const handleSubmit = (e: React.FormEvent) => {
    try {
      if (message.length > MAX_CHARACTER_LIMIT) {
        setCharacterLimitError(t('errors:characterLimit'));
        setFormErrors({});
        e.preventDefault();
      } else {
        setCharacterLimitError('');
      }

      contactFormSchema.parse({ email, message });
    } catch (error) {
      const zodError = error as ZodError;

      if (Array.isArray(zodError.errors)) {
        const newFormErrors: { email?: string; message?: string } = {};
        zodError.errors.forEach((err) => {
          if (err.path[0] === 'email') {
            newFormErrors.email = err.message;
          }
          if (err.path[0] === 'message') {
            newFormErrors.message = err.message;
          }
        });
        setFormErrors(newFormErrors);
      }
      e.preventDefault();
    }
  };

  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={onRequestClose}
      className="flex flex-col justify-center items-center lg:w-2/3 mt-48 lg:ml-48 p-10 rounded-lg main-color second-bg"
    >
      <div className="flex w-full justify-between text-3xl mb-10">
        <h2>{t('contactUs')}</h2>
        <button onClick={onRequestClose}>X</button>
      </div>
      <form
        action={'https://formspree.io/f/mdorjpld'}
        onSubmit={handleSubmit}
        className="flex flex-col w-full"
        method="POST"
      >
        <label className="flex flex-col w-48 mb-10">
          <p className="mb-1">{t('email')}:</p>
          <input
            type="email"
            placeholder={t('email')}
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            className="text-black rounded-lg p-1"
            name="Email"
          />
          {formErrors && (
            <div className="text-sm mt-2 text-red-600">
              {formErrors.email ? <p>{formErrors.email}</p> : null}
            </div>
          )}
        </label>
        <label
          className={`relative flex flex-col w-2/3 h-32 mb-5 ${
            message.length > MAX_CHARACTER_LIMIT ? 'text-red-600' : 'text-black'
          }`}
        >
          <p className="main-color mb-1">{t('message')}:</p>
          <textarea
            placeholder={t('message')}
            value={message}
            onChange={(e) => setMessage(e.target.value)}
            className="rounded-lg h-full p-1"
            name="Message"
          />
          <p className="absolute text-sm bottom-0 right-2">
            {message.length}/{MAX_CHARACTER_LIMIT}
          </p>
          {formErrors && (
            <div className="text-sm mt-2 text-red-600">
              {formErrors.message ? <p>{formErrors.message}</p> : null}
            </div>
          )}
          {characterLimitError && (
            <div className="text-sm mt-2 text-red-600">
              <p>{characterLimitError}</p>
            </div>
          )}
        </label>
        <div>
          <button type="submit" className="bg-blue-800 px-4 py-1 rounded-lg">
            {t('submit')}
          </button>
        </div>
      </form>
    </Modal>
  );
};

export default ContactModal;
