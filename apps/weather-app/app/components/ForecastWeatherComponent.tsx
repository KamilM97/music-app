import React from 'react';

interface ForecastWeatherProps {
  date: string;
  hour: string;
  icon: any;
  temp: string;
  text: string;
}

function AppListeners({ date, hour, icon, temp, text }: ForecastWeatherProps) {
  return (
    <div className="flex w-1/4 second-bg rounded-lg m-1 px-4 py-2">
      <div className="flex flex-col justify-center w-2/5">
        <p className="text-gray-400">{date}</p>
        {hour}
      </div>
      <div className="flex items-center justify-between w-3/5">
        <img className="h-4/5" src={icon} alt="Weather icon" />
        <div className="flex flex-col justify-center truncate ml-2">
          <p className="flex flex-row-reverse font-bold">{temp}°C</p>
          <p className="text-gray-400 truncate cursor-pointer" title={text}>
            {text}
          </p>
        </div>
      </div>
    </div>
  );
}

export default AppListeners;
