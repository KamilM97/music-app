import React, { useRef, useEffect, useState } from 'react';
import {
  MapContainer,
  TileLayer,
  Marker,
  useMap,
  LayersControl,
} from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import 'leaflet.locatecontrol';
import 'leaflet.locatecontrol/dist/L.Control.Locate.min.css';
import * as L from 'leaflet';

declare module 'leaflet' {
  namespace control {
    function locate(options?: any): any;
  }
}

import { CoordinatesTypes } from '@my-workspace/shared';

const LeafletMap: React.FC<CoordinatesTypes> = ({
  lat,
  lon,
  onCoordinateChange,
}) => {
  const markerRef = useRef<L.Marker | null>(null);
  const locateControlRef = useRef<any>(null);
  const mapRef = useRef<L.Map | null>(null);

  const [markerPosition, setMarkerPosition] = useState<[number, number]>([
    lat,
    lon,
  ]);

  const handleDragEnd = () => {
    const marker = markerRef.current;
    if (marker) {
      const newPosition = marker.getLatLng();

      onCoordinateChange([newPosition.lat, newPosition.lng]);
      setMarkerPosition([newPosition.lat, newPosition.lng]);
    }
  };

  useEffect(() => {
    return () => {
      if (locateControlRef.current) {
        locateControlRef.current.stop();
      }
    };
  }, []);

  const LeafletMapComponent = () => {
    const map = useMap();

    const handleMapDoubleClick = (event: L.LeafletEvent) => {
      const latlng = (event as L.LeafletMouseEvent).latlng;
      const marker = markerRef.current;
      if (marker) {
        marker.setLatLng(latlng);
        handleDragEnd();
      }
    };

    map.doubleClickZoom.disable();
    map.on('dblclick', handleMapDoubleClick);

    if (!locateControlRef.current) {
      locateControlRef.current = L.control
        .locate({
          setView: 'once',
          flyTo: true,
          strings: {
            title: 'Show my location',
          },
        })
        .addTo(map);
    }

    return null;
  };

  return (
    <>
      <MapContainer
        center={markerPosition}
        zoom={13}
        style={{ width: '100%', height: '100vh' }}
        whenReady={() => {
          if (mapRef.current) {
            mapRef.current.options.minZoom = 5;
          }
        }}
      >
        <LayersControl>
          <LayersControl.BaseLayer checked name="OpenStreetMap">
            <TileLayer
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
              attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            />
          </LayersControl.BaseLayer>

          <LayersControl.BaseLayer name="Relief Map">
            <TileLayer
              url="https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png"
              attribution='&copy; <a href="https://opentopomap.org">OpenTopoMap</a> contributors'
            />
          </LayersControl.BaseLayer>

          <LayersControl.BaseLayer name="Thunderforest">
            <TileLayer
              url="https://tile.thunderforest.com/spinal-map/{z}/{x}/{y}.png"
              attribution='&copy; <a href="https://www.thunderforest.com/legal/">Thunderforest</a> contributors'
            />
          </LayersControl.BaseLayer>
        </LayersControl>
        <Marker
          position={markerPosition}
          icon={L.icon({
            iconUrl: '/marker-icon.png',
            shadowUrl: '/marker-shadow.png',
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34],
          })}
          draggable={true}
          eventHandlers={{
            dragend: handleDragEnd,
          }}
          ref={markerRef}
        ></Marker>
        <LeafletMapComponent />
      </MapContainer>
    </>
  );
};

export default LeafletMap;
