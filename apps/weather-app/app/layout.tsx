'use client';

import { QueryClient, QueryClientProvider } from 'react-query';

import '../styles/global.css';
import React from 'react';

import Modal from 'react-modal';

const queryClient = new QueryClient();

Modal.setAppElement('#root');

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <QueryClientProvider client={queryClient}>
      <html lang="en">
        <body
          id="root"
          className="flex justify-center items-center flex-col second-bg"
        >
          {children}
        </body>
      </html>
    </QueryClientProvider>
  );
}
