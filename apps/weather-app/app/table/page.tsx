'use client';

import React, { useState, useCallback } from 'react';

import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

import Link from 'next/link';

import ContactModal from '../components/ContactModal';
import TableComponent from '../components/TableComponent';

import { useWeatherForecast } from '@my-workspace/shared';

import { useTranslation } from 'react-i18next';
import '../../i18n';

import { FaSpinner } from 'react-icons/fa';

const HoursTable = () => {
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  const [selectedDate, setSelectedDate] = useState<Date | null>(null);
  const [inputValue, setInputValue] = useState<string>('Warsaw');
  const [city, setCity] = useState<string>('Warsaw');

  const defaultDate = new Date();

  const { data, isError, isLoading } = useWeatherForecast(city, 3);

  const { t } = useTranslation('common');

  const openModal = useCallback(() => {
    setIsModalOpen(true);
  }, [isModalOpen]);

  const closeModal = useCallback(() => {
    setIsModalOpen(false);
  }, [isModalOpen]);

  const handleDateChange = useCallback(
    (date: Date) => {
      setSelectedDate(date);
    },
    [setSelectedDate]
  );

  const isDateDisabled = (date: Date) => {
    const today = new Date();
    const maxDate = new Date(today);
    maxDate.setDate(today.getDate() + 2);
    return date <= maxDate;
  };

  const daysDifference =
    selectedDate &&
    Math.floor(
      (selectedDate.getTime() - new Date().getTime()) / (1000 * 60 * 60 * 24) +
        1
    );

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.preventDefault();
    setInputValue(event.target.value);
  };

  const handleSearch = (event: React.FormEvent) => {
    event.preventDefault();
    setCity(inputValue);
  };

  if (isError) {
    return <div>{t('errorFetchingData')}.</div>;
  }

  const TableColumns = [
    { Header: 'Hour', accessor: 'hour' },
    { Header: 'Temperature (°C)', accessor: 'temp_c' },
    { Header: 'Wind speed', accessor: 'wind_kph' },
    { Header: 'Chance of rain', accessor: 'chance_of_rain' },
    { Header: 'Humidity', accessor: 'humidity' },
    { Header: 'Condition', accessor: 'icon' },
  ];

  return (
    <>
      <header className="flex justify-between rounded-t-lg main-bg w-11/12 py-6 px-10 mt-16">
        <Link href="/">
          <button className="second-bg hover:bg-gray-400 main-color hover:text-black mr-4 py-2 px-6 rounded-lg">
            {t('home')}
          </button>
        </Link>
        <div className="flex justify-between mx-4">
          <Link href="/coordinates">
            <button className="second-bg hover:bg-gray-400 main-color hover:text-black mx-2 py-2 px-6 rounded-lg">
              {t('coordinates')}
            </button>
          </Link>
          <Link href="/table">
            <button className="second-bg hover:bg-gray-400 main-color hover:text-black mx-2 py-2 px-6 rounded-lg border-2 border-white border-solid">
              {t('table')}
            </button>
          </Link>
          <Link href="">
            <button className="second-bg hover:bg-gray-400 main-color hover:text-black mx-2 py-2 px-6 rounded-lg">
              3btn
            </button>
          </Link>
          <Link href="">
            <button
              className="second-bg hover:bg-gray-400 main-color hover:text-black mx-2 py-2 px-6 rounded-lg"
              onClick={openModal}
            >
              Contact
            </button>
          </Link>
        </div>
      </header>

      <ContactModal isOpen={isModalOpen} onRequestClose={closeModal} />

      <main className="relative flex flex-col main-color rounded-b-lg main-bg w-11/12 py-6 px-10 mb-16">
        {isLoading && (
          <div className="absolute inset-0 flex items-center justify-center h-screen z-50">
            <FaSpinner className="animate-spin text-4xl text-white" />{' '}
          </div>
        )}

        <div>
          <input
            placeholder="Enter City"
            value={inputValue}
            onChange={handleInputChange}
            className="second-bg main-text mb-4 p-1 rounded-lg border-2 border-gray-400 border-solid"
          />
          <button
            onClick={handleSearch}
            className="second-bg hover:bg-gray-400 main-color hover:text-black ml-4 px-10 py-2 rounded-lg"
          >
            Search City
          </button>
        </div>

        <div>
          <DatePicker
            selected={selectedDate || defaultDate}
            onChange={handleDateChange}
            minDate={new Date()}
            filterDate={isDateDisabled}
            placeholderText="Pick Date"
            showYearDropdown
            scrollableYearDropdown
            className="second-bg main-text mb-10 p-1 rounded-lg border-2 border-gray-400 border-solid"
          />
        </div>

        <div>
          <TableComponent
            columns={TableColumns}
            data={data}
            daysDifference={daysDifference}
          />
        </div>
      </main>
    </>
  );
};

export default HoursTable;
