'use client';

import React, { useState, useCallback } from 'react';

import Link from 'next/link';

import { z } from 'zod';

import ForecastWeatherComponent from './components/ForecastWeatherComponent';
import WeatherConditionComponent from './components/WeatherConditionComponent';
import ContactModal from './components/ContactModal';

import { useWeatherForecast } from '@my-workspace/shared';

import { useTranslation } from 'react-i18next';
import '../i18n';

import { FaSpinner } from 'react-icons/fa';

const Home = () => {
  const [searchValue, setSearchValue] = useState<string>('');
  const [searchToSubmit, setSearchToSubmit] = useState<string>('Warszawa');
  const [errorMessage, setErrorMessage] = useState<string>('');
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);

  const { data, isError, isLoading } = useWeatherForecast(searchToSubmit, 3);

  const { t } = useTranslation('common');

  const allowedCharacters = z
    .string()
    .regex(/^[A-Za-z.,\b]+$/, 'Only letters, dots and commas are allowed.');

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchValue(event.target.value);
    setErrorMessage('');
  };

  const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      const result = allowedCharacters.safeParse(searchValue);

      if (result.success || searchValue === '') {
        setSearchToSubmit(searchValue);
        setErrorMessage('');
      } else {
        setErrorMessage(result.error.errors[0].message);
      }
    }
  };

  const openModal = useCallback(() => {
    setIsModalOpen(true);
  }, [isModalOpen]);

  const closeModal = useCallback(() => {
    setIsModalOpen(false);
  }, [isModalOpen]);

  if (isLoading) {
    return (
      <div className="flex items-center justify-center h-screen">
        <FaSpinner className="animate-spin text-4xl text-white" />
      </div>
    );
  }

  if (isError) {
    return <div>{t('errorFetchingData')}.</div>;
  }

  const {
    current: {
      condition: { icon, text },
      temp_c,
      wind_kph,
      pressure_mb,
      humidity,
      vis_km,
    },
    location: { name, localtime },
    forecast: {
      forecastday: [
        {
          astro: { sunrise, sunset },
        },
        { date: firstDayFirstDate, hour: firstDayHour },
        { date: secondDayFirstDate, hour: secondDayHour },
      ],
    },
  } = data;

  const firstDayHour6 = firstDayHour[6];
  const firstDayHour18 = firstDayHour[18];

  const secondDayHour6 = secondDayHour[6];
  const secondDayHour18 = secondDayHour[18];

  return (
    <>
      <header className="flex rounded-t-lg main-bg w-11/12 py-6 px-10 mt-16">
        <Link href="/">
          <button className="second-bg hover:bg-gray-400 main-color hover:text-black mr-4 py-2 px-6 rounded-lg border-2 border-white border-solid">
            {t('home')}
          </button>
        </Link>
        <div className="w-full rounded-lg px-4">
          <input
            className="second-bg main-color w-full rounded-lg px-4 py-2"
            type="text"
            placeholder="Search Weather"
            value={searchValue}
            onChange={handleInputChange}
            onKeyDown={handleKeyPress}
          ></input>
          <p className="text-red-600 mt-3">{errorMessage}</p>
        </div>
        <div className="flex justify-between mx-4">
          <Link href="/coordinates">
            <button className="second-bg hover:bg-gray-400 main-color hover:text-black mx-2 py-2 px-6 rounded-lg">
              {t('coordinates')}
            </button>
          </Link>
          <Link href="/table">
            <button className="second-bg hover:bg-gray-400 main-color hover:text-black mx-2 py-2 px-6 rounded-lg">
              {t('table')}
            </button>
          </Link>
          <Link href="">
            <button className="second-bg hover:bg-gray-400 main-color hover:text-black mx-2 py-2 px-6 rounded-lg">
              3btn
            </button>
          </Link>
          <Link href="">
            <button
              className="second-bg hover:bg-gray-400 main-color hover:text-black mx-2 py-2 px-6 rounded-lg"
              onClick={openModal}
            >
              Contact
            </button>
          </Link>
        </div>
      </header>

      <ContactModal isOpen={isModalOpen} onRequestClose={closeModal} />

      <main className="flex flex-col main-color rounded-b-lg main-bg w-11/12 py-6 px-10 mb-16">
        <h2 className="text-2xl mb-10">{t('todayOverview')}</h2>
        <WeatherConditionComponent
          icon={icon}
          temp={temp_c}
          text={text}
          name={name}
          time={localtime}
          wind={wind_kph}
          pressure={pressure_mb}
          humidity={humidity}
          vission={vis_km}
          sunrise={sunrise}
          sunset={sunset}
          firstDayFirstDate={firstDayFirstDate}
          firstDaySecondDate={firstDayFirstDate}
          secondDayFirstDate={secondDayFirstDate}
          secondDaySecondDate={secondDayFirstDate}
          firstDayFirstTemp={firstDayHour6.temp_c}
          firstDaySecondTemp={firstDayHour18.temp_c}
          secondDayFirstTemp={secondDayHour6.temp_c}
          secondDaySecondTemp={secondDayHour18.temp_c}
        />

        <div className="w-full pb-10">
          <h2 className="text-2xl my-8">{t('nextTwoDays')}</h2>
          <div className="flex flex-col w-full">
            <div className="flex pb-4">
              <ForecastWeatherComponent
                date={data.forecast.forecastday[0].date}
                hour={'23:00'}
                icon={data.forecast.forecastday[0].hour[23].condition.icon}
                temp={data.forecast.forecastday[0].hour[23].temp_c}
                text={data.forecast.forecastday[0].hour[23].condition.text}
              />
              <ForecastWeatherComponent
                date={firstDayFirstDate}
                hour={'5:00'}
                icon={firstDayHour[5].condition.icon}
                temp={firstDayHour[5].temp_c}
                text={firstDayHour[5].condition.text}
              />
              <ForecastWeatherComponent
                date={firstDayFirstDate}
                hour={'11:00'}
                icon={firstDayHour[11].condition.icon}
                temp={firstDayHour[11].temp_c}
                text={firstDayHour[11].condition.text}
              />
              <ForecastWeatherComponent
                date={firstDayFirstDate}
                hour={'17:00'}
                icon={firstDayHour[17].condition.icon}
                temp={firstDayHour[17].temp_c}
                text={firstDayHour[17].condition.text}
              />
            </div>

            <div className="flex pb-4">
              <ForecastWeatherComponent
                date={firstDayFirstDate}
                hour={'23:00'}
                icon={firstDayHour[23].condition.icon}
                temp={firstDayHour[23].temp_c}
                text={firstDayHour[23].condition.text}
              />
              <ForecastWeatherComponent
                date={secondDayFirstDate}
                hour={'5:00'}
                icon={secondDayHour[5].condition.icon}
                temp={secondDayHour[5].temp_c}
                text={secondDayHour[5].condition.text}
              />
              <ForecastWeatherComponent
                date={secondDayFirstDate}
                hour={'11:00'}
                icon={secondDayHour[11].condition.icon}
                temp={secondDayHour[11].temp_c}
                text={secondDayHour[11].condition.text}
              />
              <ForecastWeatherComponent
                date={secondDayFirstDate}
                hour={'17:00'}
                icon={secondDayHour[17].condition.icon}
                temp={secondDayHour[17].temp_c}
                text={secondDayHour[17].condition.text}
              />
            </div>
          </div>
        </div>
      </main>
    </>
  );
};

export default Home;
