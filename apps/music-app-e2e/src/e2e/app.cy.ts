import { getGreeting } from '../support/app.po';

describe('Music App', () => {
  it('should display the title', () => {
    cy.visit('/');
    cy.contains('Music App Project').should('exist');
  });
});

it('should navigate to the search page', () => {
  cy.visit('/');
  cy.contains('Search').click();
  cy.url().should('include', '/');
});

it('should display the Navbar with correct links', () => {
  cy.visit('/');
  cy.get('h2').should('contain', 'Categories');
  cy.get('ul li').should('have.length', 3);
});

describe('Music App', () => {
  it('should display "The Weeknd" as the first artist', () => {
    cy.visit('/listeners');

    cy.get('ul li:first-child [data-testid="listeners-container-1"]').should(
      'contain',
      'The Weeknd'
    );
  });
});
